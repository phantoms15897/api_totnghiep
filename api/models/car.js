const mongoose = require('mongoose');

const carSchema = mongoose.Schema({
    brand_id: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'brand'
    },
    user_id: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'user'
    },
    car_name: { 
        type: String, 
        require : ""
    },
    license_plate: { 
        type: String, 
        default : ""
    },
    color_car: { 
        type: String, 
        default : ""
    },
    slot: { 
        type: Number, 
        default : 0
    },
    city: { 
        type: String, 
        default : ""
    },
    speed: { 
        type: Number, 
        default : 0
    },
    fuel: { 
        type: String, 
        default : "0"
    },
    type: { 
        type: String, 
        default : ""
    },
    dynamic: { 
        type: Number, 
        default : 0
    },
    capacity: { 
        type: Number, 
        default : 0
    },
    function: { 
        type: Array, 
        default : []
    },
    procedure: { 
        type: Array, 
        default : []
    },
    pay: { 
        type: Array, 
        default : []
    },
    image: { 
        type: String, 
        default : ""
    },
    rate: { 
        type: Number, 
        default : 0
    },
    note: { 
        type: String, 
        default : ""
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
})
module.exports = mongoose.model('car', carSchema);