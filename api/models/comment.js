const mongoose = require('mongoose');

const commentSchema = mongoose.Schema({
    user_id: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'user'
    },
    news_id: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'news'
    },
    body: { 
        type: String, 
        default : ""
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
})
module.exports = mongoose.model('comment', commentSchema);