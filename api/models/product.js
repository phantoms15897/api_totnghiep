const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    product_id: mongoose.Schema.Types.ObjectId,
    name: String,
    price: Number,
});

module.exports = mongoose.model('Product', productSchema);