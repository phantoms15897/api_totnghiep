const mongoose = require('mongoose');

const brandSchema = mongoose.Schema({
    brand_name: { 
        type: String, 
        default : ''
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
})
module.exports = mongoose.model('brand', brandSchema);