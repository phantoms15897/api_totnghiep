const mongoose = require('mongoose');

const newsSchema = mongoose.Schema({
    user_id: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'user'
    },
    type_id: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'type_news'
    },
    title: {
        type: String,  
        default: ""
    },
    body_news: {
        type: String,  
        default: ""
    },
    views: {
        type: Number,  
        default: 0
    },
    images: {
        type: String,  
        default: ""
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
})
module.exports = mongoose.model('news', newsSchema);