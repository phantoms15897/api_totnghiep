const mongoose = require('mongoose');

const promotionSchema = mongoose.Schema({
    title: { 
        type: String, 
        default : ''
    },
    description: { 
        type: String, 
        default : ''
    },
    image: { 
        type: String, 
        default : ''
    },
    code: { 
        type: String, 
        default : ''
    },
    amount: { 
        type: Number, 
        default : 0
    },
    unit: { 
        type: String, 
        default : '%'||'VND'
    },
    date_start: { 
        type: Date, 
        default : Date.now
    },
    date_end: { 
        type: Date, 
        default : Date.now
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
})
module.exports = mongoose.model('promotion', promotionSchema);