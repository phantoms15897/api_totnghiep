const mongoose = require('mongoose');

const userdetailSchema = mongoose.Schema({
    user_id: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'user'
    },
    name: {
        type: String,
        default: null    
    },
    birthday: {
        type: Date,
        default: null    
    },
    address: {
        type: String,
        default: null    
    },
    gender: {
        type: String,
        default: null    
    },
    phone: {
        type: String,
        default: null    
    },
    email: { 
        type: String, 
        lowercase: true, 
        default:null
    },
    image: {
        type: String,
        default: null
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, 
        default: Date.now 
    }
});

module.exports = mongoose.model('user_detail', userdetailSchema);