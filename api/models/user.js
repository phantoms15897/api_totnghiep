const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    username: {
        type: String,
        lowercase: true,
        required: true,
        unique: true,
        update: false
    },
    password: {
        type: String,  
        required: true
    },
    role: {
        type: String,
        default: "user"
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
});

module.exports = mongoose.model('user', userSchema);