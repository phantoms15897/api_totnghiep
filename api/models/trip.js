const mongoose = require('mongoose');

const tripSchema = mongoose.Schema({
    car_id: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'car'
    },
    type_rent: { 
        type: String, 
        default : ''
    },
    date_start: { 
        type: Date, 
        default : Date.now
    },
    date_end: { 
        type: Date, 
        default : Date.now
    },
    location_start: { 
        type: String, 
        default : "Da Nang"
    },
    location_end: { 
        type: String, 
        default : "TPHCM"
    },
    cost: { 
        type: Number, 
        default : 0
    },
    owner:{
        type:  mongoose.Schema.Types.ObjectId, 
        ref: 'user'
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
})
module.exports = mongoose.model('trip', tripSchema);