const mongoose = require('mongoose');

const typenewsSchema = mongoose.Schema({
    type_name: { 
        type: String, 
        require : true
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
})
module.exports = mongoose.model('type_news', typenewsSchema);