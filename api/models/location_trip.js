const mongoose = require('mongoose');

const locationtripSchema = mongoose.Schema({
    trip_id_fk: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'trip',
        required: true,
    },
    location_name: { 
        type: String, 
        default : ''
    },
    lat: { 
        type: Number, 
        default : 0
    },
    long: { 
        type: Number, 
        default : 0
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
})
module.exports = mongoose.model('location_trip', locationtripSchema);