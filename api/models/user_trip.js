const mongoose = require('mongoose');

const usertripSchema = mongoose.Schema({
    user_id_fk: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'user'
    },
    trip_id_fk: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'trip'
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
})
module.exports = mongoose.model('user_trip', usertripSchema);