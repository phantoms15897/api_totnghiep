const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Location_Trip = require("../models/location_trip")

//List location trip all
router.get('/', (req,res,next)=>{
    Location_Trip
    .find()
    .exec()
    .then(location_trip_list => {
        if(location_trip_list.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: location_trip_list
            });
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});

//Create Location Trip
router.post('/', (req,res,next)=>{
    var location_name = req.body.location_name;
    console.log(req.body.location_name);
    if(location_name != null && location_name.length > 0){
        Location_Trip
        .find({location_name: location_name})
        .exec()
        .then(result => {
            console.log(result);
            if(result.length == 0){
                const one_location_trip = new Location_Trip({
                    trip_id_fk: req.body.trip_id_fk,
                    location_name: req.body.location_name,
                    lat: req.body.lat,
                    long: req.body.long,
                });
                one_location_trip
                .save()
                .then(result => {
                    console.log(result);
                    res.status(200).json({
                        status: "SUCCESS"
                    })
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).json({
                        status: "FAIL"
                    });
                });
            }else{
                res.status(500).json({
                    status: "FAIL"
                });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                status: "FAIL"
            });
        });
    }else{
        res.status(200).json({
            status: "FAIL"
        })
    }
});
router.get('/:locationtripId',(req,res,next)=>{
    const id = req.params.locationtripId;
    res.status(200).json({
        message: "Location_Trip Method Get" + id
    })
});

router.patch('/:locationtripId',(req,res,next)=>{
    const id = req.params.locationtripId;
    res.status(200).json({
        message: "Location_Trip Method patch" + id
    })
});

router.delete('/:locationtripId',(req,res,next)=>{
    const id = req.params.locationtripId;
    Location_Trip.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Location deleted"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
module.exports = router;