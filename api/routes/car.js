const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Car = require("../models/car")

router.get('/', (req,res,next)=>{
    let page = req.query.page;
    page = page - 1;
    console.log(page);
    Car
    .find({}).sort({_id : -1}).skip(page*10).limit(10)
    .exec()
    .then(car_list => {
        if(car_list.length >= 0)
        {   
            Car.find().exec().then(result=>{
                res.status(200).json({
                    status: "SUCCESS",
                    data: car_list,
                    metadata: {
                        page: page+1,
                        total: result.length
                    }  
                });
            })
            
        }else{
            res.status(404).json({
                status: "FAIL",
            })
        }
        
    })
    .catch(err =>{
        res.status(500).json({
            error: err
        })
    });
});
//Create Car
router.post('/', (req,res,next)=>{
    const one_car = new Car({
        brand_id: req.body.brand_id,
        user_id: req.body.user_id,
        car_name: req.body.car_name,
        license_plate: req.body.license_plate,
        color_car: req.body.color_car,
        slot: req.body.slot,
        city: req.body.city,
        speed: req.body.speed,
        fuel: req.body.fuel,
        type: req.body.type,
        rate: req.body.rate,
        dynamic: req.body.dynamic,
        image: req.body.image,
        capacity: req.body.capacity,
        fuel: req.body.fuel,
        function: req.body.function,
        procedure: req.body.procedure,
        pay: req.body.pay,
        note: req.body.note
    });
    one_car
    .save()
    .then(result => {
        console.log(result);
        res.status(201).json({
            status: "SUCCESS"
        })
    })
    .catch(err => {
        res.status(500).json({
            status: "FAIL",
        });
    });
});

//Search Car
router.get('/:carId',(req,res,next)=>{
    const id = req.params.carId;
    Car
    .find({ _id: id})
    .exec()
    .then(car_list_search => {
        if(car_list_search.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: car_list_search[0]
            });
        }else{
            res.status(404).json({
                status: "FAIL",
            })
        }
        
    })
    .catch(err =>{
        res.status(500).json({
            error: err
        })
    });
});

router.patch('/:carId',(req,res,next)=>{
    const id = req.params.carId;
    console.log(id);
    Car.updateOne({ _id: id }, { $set: req.body })
        .exec()
        .then(result => {
            Car
            .find({_id: id })
            .exec()
            .then(one_car =>{
                if(one_car.length > 0){
                    res.status(200).json({
                        status: "SUCCESS",
                        data: one_car[0]
                    });
                }
            })            
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
});

router.delete('/:carId',(req,res,next)=>{
    const id = req.params.carId;
    Car.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
});
module.exports = router;