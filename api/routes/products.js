const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Product = require("../models/product")
const ObjectID = require('bson').ObjectID;

router.get('/', (req,res,next)=>{
    Product
    .find()
    
    .exec()
    .then(docs => {
        delete docs[0]["_id"];
        if(docs.length >= 0)
        {
            res.status(200).json(docs);
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
    
});
router.post('/', (req,res,next)=>{
    const product = new Product({
        product_id: new ObjectID(),
        name: req.body.name,
        price: req.body.price
    });
    product
    .save()
    .then(result =>{
        console.log(result._id);
        res.status(201).json({
            message: 'Handling POST request to /products',
            createdProduct: product
        });
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
    
});
router.get('/:productId',(req,res,next)=>{
    const id = req.params.productId;
    Product
        .findById(id)
        .exec()
        .then(doc => {
            console.log("From database", doc);
            if(doc){
                res.status(200).json(doc);
            }else{
                res.status(404).json({
                    message: "Not Found"
                });
            }
            
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                error: err
            })        
        });
    // if (id === 'special'){
    //     res.status(200).json({
    //         message: "You discovered the special ID",
    //         id: id
    //     });
    // }
    // else{
    //     res.status(200).json({
    //         message: "You passed an ID"
    //     })
    // }
});

router.patch('/:productId',(req,res,next)=>{
    const id = req.params.productId;
    const updateOps = {};
    for (const ops of req.body){
        updateOps[ops.propName] = ops.value;
    }
    Product.update({_id: id},{$set: updateOps})
    .exec()
    .then(result =>{
        console.log(result);
        res.status(200).json(result);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            message: "Updated product!"
        })
    });
    
    
});

router.delete('/:productId',(req,res,next)=>{
    const id = req.params.productId;

    Product.remove({_id: id})
    .exec()
    .then(result =>{
        res.status(200).json(result);
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
    res.status(200).json({
        message: "Deleted product!"
    })
    
});

module.exports = router;