const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User_Trip = require("../models/user_trip")

//User Trip List
router.get('/', (req,res,next)=>{
    User_Trip
    .find()
    .exec()
    .then(user_trip_list => {
        if(user_trip_list.length > 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: user_trip_list
              }
            );
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});

//Create User Trip
router.post('/', (req,res,next)=>{
    const one_user_trip = new User_Trip({
        user_id: req.body.user_id,
        trip_id: req.body.trip_id,
    });
    one_user_trip
    .save()
    .then(result => {
        console.log(result);
        res.status(201).json({
            status: "SUCCESS"
        })
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
        error: err
        });
    });
});
//Chưa Xong
//Search user on trip
router.get('/:tripId',(req,res,next)=>{
    // const id = req.params.tripId;
    // User.aggregate([{
    //     $lookup: {
    //         from: 'user_trip',
    //         localField: '_id',
    //         foreignField: 'user_id',
    //         as: 'user_on_trip'
    //     }
    // }])
    // .exec()
    // .then(user => {
    //     if (user.length >= 0) {
    //         res.status(200).json({
    //             status: "SUCCESS",
    //             data: user
    //         });
    //     } else {
    //         res.status(404).json({
    //             status: "FAIL"
    //         })
    //     }

    // })

});

router.patch('/:usertripId',(req,res,next)=>{
    const id = req.params.usertripId;
    User_Trip
        .updateOne({ _id: id }, { $set: req.body })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.delete('/:usertripId',(req,res,next)=>{
    const id = req.params.usertripId;
    User_Trip.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
module.exports = router;