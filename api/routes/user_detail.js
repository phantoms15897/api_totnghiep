const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const User_detail = require("../models/user_detail");

router.get("/", (req, res, next) => {
  User_detail.aggregate([
    {
      $lookup: {
        from: "users",
        localField: "user_id",
        foreignField: "_id",
        as: "users"
      }
    }
  ])
    // .where('_id').eq
    .exec()
    .then(user_details => {
      if (user_details.length >= 0) {
        res.status(200).json({
          status: "SUCCESS",
          data: user_details
        });
      } else {
        res.status(404).json({
          message: "No entries found"
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});
router.post("/", (req, res, next) => {
  res.status(200).json({
    message: "user_detail Method post"
  });
});
router.get("/:userId", (req, res, next) => {
  const id = req.params.userId;
  res.status(200).json({
    message: "user_detail Method Get" + id
  });
});

router.patch("/:userId", (req, res, next) => {
  const id = req.params.userId;
  res.status(200).json({
    message: "user_detail Method patch" + id
  });
});

router.delete("/:userId", (req, res, next) => {
  const id = req.params.userId;
  res.status(200).json({
    message: "user_detail Method delete" + id
  });
});
module.exports = router;
