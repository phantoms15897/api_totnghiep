const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Comment = require("../models/comment");
const News = require("../models/news");


//List Comment All
router.get('/', (req,res,next)=>{
    Comment
    .find()
    .exec()
    .then(comment_list => {
        if(comment_list.length >= 0)
        {
            res.status(200).json(
                {
                    status: "SUCCESS",
                    data: comment_list
                });
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});

//Post Comment
router.post('/', (req,res,next)=>{
    const one_comment = new Comment({
        user_id: req.body.user_id,
        news_id: req.body.news_id,
        body: req.body.body
    });
    one_comment
    .save()
    .then(result => {
        console.log(result);
        res.status(201).json({
            status: "SUCCESS"
        })
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
        error: err
        });
    });    
});

//Find Comment in news
router.get('/:newsId',(req,res,next)=>{
    const id = req.params.newsId;
    Comment
    .find({
        news_id:id
    })
    .exec()
    .then(comment_list => {
        if(comment_list.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: comment_list
            });
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});
//Update Comment
router.patch('/:commentId',(req,res,next)=>{
    const id = req.params.commentId;
    console.log(id);
    Comment.updateOne({ _id: id }, { $set: req.body })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
//Delete Comment
router.delete('/:commentId',(req,res,next)=>{
    const id = req.params.commentId;
    Comment.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
module.exports = router;