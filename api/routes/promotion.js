const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Promotion = require("../models/promotion");

//Find List Promotion All
router.get('/', (req,res,next)=>{
    let page = req.query.page;
    page = page - 1;
    console.log(req.query.page);
    Promotion
    .find({}).limit(10).skip(page)
    .exec()
    .then(list_promotion => {
        if(list_promotion.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: list_promotion
            });
        }else{
            res.status(404).json({
                status: "FAIL"
            });
        }
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            status: "FAIL"
        })
    });
});

//Create Promotion
router.post('/', (req,res,next)=>{
    const one_promotion = new Promotion({
        title: req.body.title,
        description: req.body.description,
        image: req.body.image,
        code: Math.floor(Math.random() * Math.floor(999999)),
        amount: req.body.amount,
        unit: req.body.unit,
        date_end: req.body.date_end,
        date_start: req.body.date_start,
    });
    one_promotion
    .save()
    .then(result => {
        console.log(result);
        res.status(201).json({
            status: "SUCCESS"
        })
    })
    .catch(err => {
        res.status(500).json({
            status: "FAIL",
        });
    });
});

//Find promotion by id
router.get('/:promotionId',(req,res,next)=>{
    let id = req.params.promotionId;
    Promotion
    .find({_id: id})
    .exec()
    .then(list_promotion => {
        if(list_promotion.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: list_promotion[0]
            });
        }else{
            res.status(404).json({
                status: "FAIL"
            });
        }
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            status: "FAIL"
        })
    });
});
//Update Trip
router.patch('/:promotionId',(req,res,next)=>{
    const id = req.params.promotionId;
    Promotion.updateOne({ _id: id }, { $set: req.body })
    .exec()
    .then(result => {
        res.status(200).json({
            status: "SUCCESS"
        });
    })
    .catch(err => {
        res.status(500).json({
            status: "FAIL",
        });
    });
});


//Delete Trip
router.delete('/:promotionId',(req,res,next)=>{
    const id = req.params.promotionId;
    Promotion.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
});
module.exports = router;