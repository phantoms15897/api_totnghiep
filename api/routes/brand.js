const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Brand = require("../models/brand")

//List Brand
router.get('/', (req,res,next)=>{
    Brand
    .find()
    .exec()
    .then(brand_list => {
        if(brand_list.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data : brand_list
            });
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});
//Create brand
router.post('/', (req,res,next)=>{
    // const one_brand = new Brand({
    //     brand_name: req.body.brand_name,
    // });
    Brand
    .find({brand_name: req.body.brand_name})
    .exec()
    .then(result=>{
        console.log(result);
        if(result.length == 0){
            const one_brand = new Brand({
                    brand_name: req.body.brand_name,
            });
            one_brand
            .save()
            .then(result => {
                // console.log(result);
                res.status(201).json({
                    status: "SUCCESS"
                })
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                error: err
                });
            });
        }
        else{
            res.status(201).json({
                status: "FAIL"
            })
        }
    })
    .catch(err => {
        res.status(500).json({
            status: "FAIL"
        });
    }); 
});
//Search brand name
router.get('/:brandname',(req,res,next)=>{
    const brandname = req.params.brandname;
    Brand
    .find({ brand_name: brandname})
    .exec()
    .then(brand_list_search => {
        if(brand_list_search.length >= 0)
        {
            res.status(200).json(brand_list_search);
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});

router.patch('/:brandId',(req,res,next)=>{
    const id = req.params.brandId;
    Brand.updateOne({ _id: id }, { $set: req.body })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
//Delete brand
router.delete('/:brandId',(req,res,next)=>{
    const id = req.params.brandId;
    Brand.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
module.exports = router;