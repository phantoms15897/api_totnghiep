const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Trip = require("../models/trip")

//Find List Trip All
router.get('/', (req,res,next)=>{
    let page = req.query.page;
    page = page - 1;
    // console.log(req.query);
    Trip.aggregate([
        {
            $lookup: {
                from: 'location_trips',
                localField: '_id',
                foreignField: 'trip_id_fk',
                as: 'locations'
            },
        },
        {
            $lookup: {
                from: 'user_trips',
                localField: '_id',
                foreignField: 'trip_id',
                as: 'user_trips',
            }
        },
        {
            $skip: 10*page
        },
        {
            $limit: 10,
        },        
    ])
    .exec()
    .then(trips => {
        if (trips.length >= 0) {
            Trip.find({})
            .exec()
            .then(results=>{
                res.status(200).json({
                    status: "SUCCESS",
                    data: trips,
                    metadata: {
                    page: page+1,
                    total: results.length
                    }                   
                });
            })
            
        } else {
            res.status(404).json({
                status: "FAIL"
            })
        }

    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            status: "FAIL",
            error: err
        })
    });
});

//Create Trip
router.post('/', (req,res,next)=>{
    const one_trip = new Trip({
        car_id: req.body.car_id,
        type_rent: req.body.type_rent,
        date_start: req.body.date_start,
        date_end: req.body.date_end,
        location_start: req.body.location_start,
        cost: req.body.cost,
        owner: req.body.owner
    });
    one_trip
    .save()
    .then(result => {
        console.log(result);
        res.status(201).json({
            status: "SUCCESS"
        })
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            status: "FAIL"
        });
    }); 
});

// Find Trip by id_car
router.get('/getid',(req,res,next)=>{
    let id_car = mongoose.Types.ObjectId(req.query.id_car);
    Trip
    .find({car_id: id_car})
    .exec()
    .then(trip_list_search => {
        if(trip_list_search.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: trip_list_search
            });
        }else{
            res.status(404).json({
                status: "FAIL"
            })
        }
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            status: "FAIL"
        })
    });
});
//Find Trip by name
router.get('/:location_start/:location_end',(req,res,next)=>{
    const location_s = req.params.location_start;
    const location_e = req.params.location_end;
    Trip
    .find({location_start: location_s,location_end: location_e })
    .exec()
    .then(trip_list_search => {
        if(trip_list_search.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: trip_list_search
            });
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});
//GET Trip by id
router.get('/:trip',(req,res,next)=>{
    let id = req.params.trip;
    console.log(id);
    Trip
    .find({_id: id})
    .exec()
    .then(trip_list_search => {
        if(trip_list_search.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: trip_list_search[0]
            });
        }else{
            res.status(404).json({
                status: "FAIL"
            });
        }
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            status: "FAIL"
        })
    });
});
//Update Trip
router.patch('/:tripId',(req,res,next)=>{
    const id = req.params.tripId;
    Trip.updateOne({ _id: id }, { $set: req.body })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});


//Delete Trip
router.delete('/:tripId',(req,res,next)=>{
    const id = req.params.tripId;
    Trip.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
module.exports = router;