const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const multer = require('multer');

const storage = multer.diskStorage({
    destination: function(req,file, cb){
        cb(null,'./uploads/')
    },
    filename: function(req,file,cb){
        cb(null, new Date().toISOString() + file.originalname);
    }
});
const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
      },
    fileFilter: fileFilter
});
const checkAuth = require('../midlleware/check-auth');

const User = require("../models/user")
const User_Detail = require("../models/user_detail")

//List User
router.get('/', (req, res, next) => {
    let page = req.query.page;
    page = page - 1;
    // console.log(req.query);
    User.aggregate([
        {
            $lookup: {
                from: 'user_details',
                localField: '_id',
                foreignField: 'user_id',
                as: 'user_details'
            }
        },
        {
            $sort:{ created_at: -1}  
        },
        {
            $skip: 10*page
        },
        {
            $limit: 10,
        },
        {
            $unwind: "$user_details"
        },
        
    ])
        .exec()
        .then(user => {
            if (user.length >= 0) {
                User.find({})
                .exec()
                .then(results=>{
                    res.status(200).json({
                        status: "SUCCESS",
                        data: user,
                        metadata: {
                        page: page+1,
                        total: results.length
                       }                   
                    });
                })
                
            } else {
                res.status(404).json({
                    status: "FAIL"
                })
            }

        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                status: "FAIL",
                error: err
            })
        });

});
//Find User
router.get('/:userid', (req, res, next) => {
    User
        .find({ _id: req.params.userid }).populate("user_details")
        .exec()
        .then(user => {
            if (user.length >= 0) {
                let u = user[0].toObject();
                User_Detail.find({ user_id: req.params.userid })
                    .exec()
                    .then(user_details => {
                        u.user_details = user_details[0];
                        // console.log(typeof(tmp));
                        console.log(u);
                        res.status(200).json({
                            status: "SUCCESS",
                            data: u
                        });
                    })

            } else {
                res.status(404).json({
                    status: "FAIL"
                })
            }

        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                status: "FAIL",
                error: err
            })
        });

});
//Update User Detail
var hashPassword = async function(password){
    // console.log(bcrypt.hash(password,10));
    return hashPwd = await bcrypt.hash(password,10);
    // console.log(hashPwd);
}
router.patch('/:userid',async (req, res, next) => {
    const id = req.params.userid;
    var update_user = {};
    if(req.body.password){
        const pwd = await hashPassword(req.body.password);
        update_user = {
            password: pwd,
            role: req.body.role
        }
    }else{
        update_user = {
            role: req.body.role
        }
    }
    
    console.log(req.body);
    User
    .updateOne(
        { _id: id },
        { $set:  update_user}
    )
    .exec()
    .then(result => {
        User_Detail.updateOne({ user_id: id }, { $set: req.body })
        .exec()
        .then(results => {
            User
            .find({ _id: req.params.userid }).populate("user_details")
            .exec()
            .then(user => {
                if (user.length >= 0) {
                    let u = user[0].toObject();
                    User_Detail.find({ user_id: req.params.userid })
                        .exec()
                        .then(user_details => {
                            u.user_details = user_details[0];
                            res.status(200).json({
                                status: "SUCCESS",
                                data: u
                            });
                        })

                } else {
                    res.status(404).json({
                        status: "FAIL"
                    })
                }

            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    status: "FAIL",
                    error: err
                })
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                status: "FAIL",
                error: err
            });
        });
    })
    
});
//Sigup
router.post('/sigup',upload.single('image'), (req, res, next) => {
    User
        .find({
            username: req.body.username
        })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                return res.status(200).json({
                    status: "FAIL",
                    message: "Account exists",
                    exist: true
                });
            }
            else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            error: err
                        });
                    } else {
                        if(req.file){  
                            file_path = req.file.path;
                        } 
                        else {
                            file_path = "";
                        }
                        const user = new User({
                            username: req.body.username,
                            password: hash,
                            role: req.body.role,
                        });
                        user
                        .save()
                        .then(result => {
                            console.log(file_path);
                            const user_detail = new User_Detail({
                                user_id: result._id,
                                name: req.body.name,
                                birtday: req.body.birtday,
                                address: req.body.address,
                                gender: req.body.gender,
                                phone: req.body.phone,
                                email: req.body.email,
                                image: file_path
                            })
                            user_detail
                            .save()
                            .then(result2=>{
                                res.status(500).json({
                                    status: "FAIL",
                                    error: err
                                });
                            })
                            .catch(err => {
                                console.log("create user 1");
                            });
                            res.status(200).json({
                                status: "SUCCESS"
                            })

                        })
                        .catch(err => {
                            // console.log(err);
                            console.log("create user 2");
                            res.status(500).json({
                                status: "FAIL",
                                error: err
                            });
                        });
                    }
                });
            }

        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                status: "FAIL",
                error: err
            })
        });

});

//Login
router.post('/login', async (req, res, next) => {
    User
        .find({ username: req.body.username })
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({
                    status: "FAIL",
                    message: "Auth failed",
                });
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        status: "FAIL",
                        message: "Auth failed"
                    });
                }
                if (!result) {
                    return res.status(401).json({
                        status: "FAIL",
                        message: "Auth failed"
                    });
                }
                const token = jwt.sign(
                    {
                        username: user[0].email,
                        userId: user[0]._id
                    },
                    "secret",
                    {
                        expiresIn: "1h"
                    }
                );
                User_Detail
                    .find({ user_id: user[0]._id })
                    .exec()
                    .then(user_detail_result => {
                        return res.status(200).json({

                            status: "SUCCESS",
                            data: {
                                token: token,
                                user_data: {
                                    user_details: user_detail_result[0],
                                    user: user[0]
                                }
                            }
                        });
                    })

            });

        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                status: "FAIL",
                error: err
            })
        });

});

//Delate User
router.delete('/:userid', (req, res, next) => {
    var id = req.params.userid;
    console.log(id);
    User.remove({ _id: id })
        .exec()
        .then(result => {
            User_Detail.remove({ user_id: id })
                .exec()
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                status: "FAIL",
                error: err
            });
        });
});

module.exports = router;