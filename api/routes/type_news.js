const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Type_News = require("../models/type_news")
//List type news
router.get('/', (req,res,next)=>{
    Type_News
    .find()
    .exec()
    .then(list_type_news => {
        if(list_type_news.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: list_type_news
            });
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});
//Create news
router.post('/', (req,res,next)=>{
    Type_News
    .find({ 
        type_name: req.body.type_name
    })
    .exec()
    .then(type_names => {
        if(type_names.length >= 1)
        {
            return res.status(409).json({
                message: "Type name exists"
            });
        }
        else
        {
            const one_type_news = new Type_News({
                type_name: req.body.type_name,
            });
            one_type_news
            .save()
            .then(result => {
                console.log(result);
                res.status(201).json({
                    status: "SUCCESS"
                })
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                error: err
                });
            });
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});

//Search type news
router.get('/:typenewsId',(req,res,next)=>{
    const id = req.params.typenewsId;
    Type_News
    .find({_id: req.params.typenewsId})
    .exec()
    .then(one_type => {
        if(one_type.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: one_type
            });
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});

router.patch('/:typenewsId',(req,res,next)=>{
    const id = req.params.typenewsId;
    Type_News.updateOne({ _id: id }, { $set: req.body })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

//Delete typenews
router.delete('/:typenewsId',(req,res,next)=>{
    const id = req.params.typenewsId;
    Type_News.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
module.exports = router;