const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const News = require("../models/news")

router.get('/', (req,res,next)=>{
    News
    .find()
    .exec()
    .then(news_list => {
        if(news_list.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: news_list
            });
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});
router.post('/', (req,res,next)=>{
    News
    .find({ 
        title: req.body.title
    })
    .exec()
    .then(titles => {
        if(titles.length >= 1)
        {
            return res.status(409).json({
                message: "Title News exists"
            });
        }
        else
        {
            const one_news = new News({
                user_id: req.body.user_id,
                type_id: req.body.type_id,
                title: req.body.title,
                body_news: req.body.body_news,
                images: req.body.images
            });
            one_news
            .save()
            .then(result => {
                console.log(result);
                res.status(201).json({
                    status: "SUCCESS"
                })
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                error: err
                });
            });
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});
router.get('/:newsId',(req,res,next)=>{
    const id = req.params.newsId;
    News
    .find({_id: req.params.newsId})
    .exec()
    .then(one_news => {
        if(one_news.length >= 0)
        {
            res.status(200).json({
                status: "SUCCESS",
                data: one_news
            });
        }else{
            res.status(404).json({
                message: "No entries found"
            })
        }
        
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});

router.patch('/:newsId',(req,res,next)=>{
    const id = req.params.newsId;
    // console.log(req.body);
    News.updateOne({ _id: id }, { $set: req.body })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.delete('/:newsId',(req,res,next)=>{
    const id = req.params.newsId;
    News.remove({ _id: id })
        .exec()
        .then(result => {
            // User_Detail.remove({ _id: req.params.userId })
            // .exec()
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
module.exports = router;