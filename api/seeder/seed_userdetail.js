// let items = [];
// for(i=0; i < 15; i++){
//     items.push(
//         {
//             name : faker.name.findName(),
//             birthday : faker.internet.password(),
//             address : faker.random.number(1,4),
//             gender: "man",
//             phone: faker.phone.phoneNumber(),
//             email: faker.internet.email(),
//             image : faker.image.image()
//         }
//     )
// }

var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const seeder = require('mongoose-seed');
const async = require('async');

const User = require('../models/users');
const _ = require('lodash');
var faker = require('faker');

new Promise((resolve) => {
    mongoose.connect('mongodb://localhost:27017/api_nodejs', {
        promiseLibrary: require('bluebird'),
        useNewUrlParser: true 
    });
    async.parallel([
        (callback) => {
            User.find({})
            .exec((err, user_ids) => {
                callback(null, user_ids);
            });
        }
    ], 
    (err, results) => {
        resolve(results);
        mongoose.connection.close();
    });
})
.then((results) => {
    
    return new Promise((resolve) => {
        let items = [];
        let status = [1, 2]
        for(i=0; i< 15; i++){
            items.push(
                {
                    name : faker.name.findName(),
                    birthday : faker.date.recent(),
                    address : faker.random.number(1,4),
                    gender: "man",
                    phone: faker.phone.phoneNumber(),
                    email: faker.internet.email(),
                    image : faker.image.image(),
                    user_id : _.sample(results[0])._id
                }
            );
        }
        resolve(items);
        // console.log(items);
    });
    
})
.then((items) => {
    // seeder.connect('mongodb://localhost:27017/api_nodejs', function() {
        // let data = [{
        //     'model': 'user_detail',
        //     'document': items
        // }]
    //     seeder.loadModels([
    //         '../models/user_detail'
    //     ]);
        let db = mongoose.connection.db;
        db.collection('user_detail').insertMany(items);
        // seeder.clearModels(['user_detail'], function() {
        //     seeder.populateModels(data, function() {
        //     seeder.disconnect();
        //     });
        // });
    //  });
});
