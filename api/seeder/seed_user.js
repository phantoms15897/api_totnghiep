const seeder = require('mongoose-seed');
const faker = require('faker');

let items = [];
for(i=0; i < 15; i++){
    items.push(
        {
            username : faker.name.findName(),
            password : faker.internet.password(),
            role : faker.random.number(4)
        }
    )
}

let data = [{
    'model': 'user',
    'documents': items
}]

// connect mongodb
seeder.connect('mongodb://localhost:27017/api_nodejs', function() {
  seeder.loadModels([
    '../models/user'  // load mongoose model 
  ]);
  seeder.clearModels(['user'], function() {
    seeder.populateModels(data, function() {
      seeder.disconnect();
    });
  });
});