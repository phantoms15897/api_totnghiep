const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//Import Route
const productRouters = require('./api/routes/products');
const orderRouters = require('./api/routes/orders');
//Admin
const adminRouters = require('./api/routes/admin');
//Module User
const userRouters = require('./api/routes/user');
const userdetailRouters = require('./api/routes/user_detail');
//Module News
const newsRouters = require('./api/routes/news');
const commentRouters = require('./api/routes/comment');
const typenewsRouters = require('./api/routes/type_news');
//Module Car & Trip
const carRouters = require('./api/routes/car');
const locationtripRouters = require('./api/routes/location_trip');
const brandRouters = require('./api/routes/brand');
const tripRouters = require('./api/routes/trip');
const usertripRouters = require('./api/routes/user_trip');
const promotionRouters = require('./api/routes/promotion');
var cors = require('cors');

const multer = require('multer');

const storage = multer.diskStorage({
    destination: function(req,file, cb){
        cb(null,'./uploads/')
    },
    filename: function(req,file,cb){
        cb(null, Math.random(999999).toString() + file.originalname);
    }
});
const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
      },
    fileFilter: fileFilter
}).array('image',4);;

mongoose.connect('mongodb://localhost:27017/api_nodejs',{ useNewUrlParser: true })
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//enables cors
app.use(cors({
    // 'allowedHeaders': ['sessionId', 'Content-Type'],
    // 'exposedHeaders': ['sessionId'],
    'origin': '*',
    'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'preflightContinue': false
  }));
app.use((req,res,next)=>{
    res.header('Access-Control-Allow-Origin','*');
    res.header(
        'Access-Control-Allow-Header',
        'Origin, X-Requested-With, Content-ype,Accept,Authorization'
    );
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods','PUT,POST,PATCH,DELETE,GET');
        return res.status(200).json({

        })
    }
    next();
})
//Routes which should handle requests
app.use('/uploads',express.static(__dirname+'/uploads'))
app.post('/uploads/multi',async (req, res, next)=>{
        upload(req,res,function(err) {
            if(err) {
                return res.status(200).json({
                    status: "FAIL"
                });
            }
            if(req.files.length>0){
                let ar_image = req.files;
                let path_ar_image = [];
                for(let key in ar_image){
                    path_ar_image.push(ar_image[key].path);
                }
                res.status(200).json({
                    status: "SUCCESS",
                    data: path_ar_image
                });
            }else{
                res.status(500).json({
                    status: "FAIL"
                });
            }
            
        });
});
app.use('/products', productRouters);
app.use('/orders', orderRouters);
app.use('/promotion',promotionRouters);
app.use('/admin', userRouters);
//Module User
app.use('/user', userRouters);
app.use('/user_detail', userdetailRouters)
//Module News
app.use('/news', newsRouters);
app.use('/comment', commentRouters);
app.use('/type_news', typenewsRouters);
//Module Car &Trip
app.use('/car', carRouters);
app.use('/brand', brandRouters);
app.use('/trip', tripRouters);
app.use('/location_trip', locationtripRouters);
app.use('/user_trip', usertripRouters);



app.use((req,res,next)=>{
    const error = new Error("Not Found");
    error.status = 404;
    next(error);
});
app.use((error,req,res, next)=>{
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
});

module.exports = app;
